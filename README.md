# AETTUA k8s Workshop

This repository is a starting point for the workshop

So grab your [tools 🛠](#-tools) and let's start 🚀

## Plan:
### Phase 1: CI/CD with Gitlab
🎬: Fork this repository and clone it to your computer  
🚧: Add the new stages to `.gitlab-ci.yml file` [documentation](https://docs.gitlab.com/ee/ci/yaml/)  
- Merge request - Build, Lint & Test
- Master branch - Build, List, Test, Docker & Deploy

### Phase 2: Docker and Kubernetes
🔨: Go to [this codelab](https://codelabs.developers.google.com/codelabs/cloud-hello-kubernetes/index.html?index=../..index#3) and continue from that step  

Integrate this scripts into the file you create in the phase 1.

[Checkout these steps](#integrate-gitlab-ci-with-kubectl)

### Phase 3: Helm
📦: Create a helm chart to help with the deployment  
- Run `helm create chart`
- In `chart.yaml` change `name` 
- In `values.yaml`, change `repository.image`, `service.type` to `LoadBalancer` and `service.port` to `8080`
- `helm install`
- Test

### Phase 4: Merge everything!!!
🔮: Let's finish the magic and glue it all  


## 🛠 Tools

- [Gitlab account](#-gitlab-account)
- [Google Cloud account](#-google-cloud-account)
- [NodeJS ready environment](#-nodejs)
- [Google Cloud CLI](#-google-cloud-cli)
- [Docker](#-docker)
- [Kubectl](#-kubectl)


### 🥼 Gitlab account

[Create an account](https://gitlab.com/users/sign_up) or [Sign in](https://gitlab.com/users/sign_in)

### ⛈ Google Cloud account

[Create an account](https://accounts.google.com/signup/v2/webcreateaccount?service=cloudconsole&continue=https%3A%2F%2Fconsole.cloud.google.com%2F&dsh=S302562631%3A1582572662895492&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp&nogm=true) or [Sign in](https://accounts.google.com/signin/v2/identifier?service=cloudconsole&passive=1209600&osid=1&continue=https%3A%2F%2Fconsole.cloud.google.com%2F&followup=https%3A%2F%2Fconsole.cloud.google.com%2F&flowName=GlifWebSignIn&flowEntry=ServiceLogin)

### 🤮 NodeJS

Linux: `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash && nvm install --lts`  
macOS: `brew install nvm && nvm install --lts`  
Windows: https://nodejs.org/en/download/package-manager/#windows  

### 🤷 Google Cloud CLI

https://cloud.google.com/sdk/install#installation_options  

macOS: `brew cask install google-cloud-sdk`

#### After Instalation

To be able to use gcloud, you have to authenticate with google cloud on your computer.

Please run `gcloud auth login` on your computer and login with your account.

### 📦 Docker

Linux: https://docs.docker.com/install/  
macOS: `brew cask install docker`  
Windows: https://hub.docker.com/editions/community/docker-ce-desktop-windows/  

### 👮 Kubectl

Linux: https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-using-native-package-management  
macOS: `brew install kubectl `  
Windows: https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-binary-with-curl-on-windows  


### Integrate gitlab-ci with kubectl

Go to service accounts on IAM

![Go to service accounts on IAM](https://gitlab.com/portellaa/aettua-k8s-workshop/-/raw/master/assets/1.png)

![Select the service account](https://gitlab.com/portellaa/aettua-k8s-workshop/-/raw/master/assets/2.png)

![Edit to enable key creation](https://gitlab.com/portellaa/aettua-k8s-workshop/-/raw/master/assets/3.png)

![Download as JSON](https://gitlab.com/portellaa/aettua-k8s-workshop/-/raw/master/assets/4.png)
